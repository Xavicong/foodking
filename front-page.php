<?php 
 /*
 Template Name: Home
 */   
get_header(); 

?>
<section class="banner">
        <div class="banner-box">
        <div class="owl-carousel owl-theme">
            <?php if( have_rows('slides')):?>
                <?php while( have_rows('slides')) : the_row();
                    $image = get_sub_field('image');
                    $imageurl = $image['sizes']['slider']
                ?>
                  <div>  <img src="<?php echo $imageurl ;?>"/></div>
                   
                <?php endwhile; ?>   
             <?php endif; ?> 
            
              

                </div>
        </div>
</section>



<section class="categories">

     <?php //echo do_shortcode('[product_categories number="12" ids="21,17"]')?>

</section>


<section class="shop-section">
    <h2 class="shop-section__heading text-center">NEW PRODUCTS</h2>
    <!-- <p class="shop-section__body">New arrivals. Updated every<br/> day. It's time to explore.</p> -->
    <div class="items">
        <?php echo do_shortcode('[products limit="16" orderbyid="id" order="DESC"]'); ?>
    </div>
</section>


<?php get_footer(); ?>