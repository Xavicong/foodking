<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package nikereal
 */

?>

	<footer id="colophon" class="site-footer">
	
		<div class="site-info">
			<div class="site-info__payment">
				<div class="copy-right">
					Copyright © 2016 order.chmzw.com All Rights Reserved
				</div>
				<div class="copy-right">
					全国免费客服电话：<a href="tel:13800000000">13800000000</a>
				</div>
				
			</div>	
<!-- 			<div class="site-info__menu">
				<?php
// 					wp_nav_menu(array( 
// 						'theme_location' => 'my-custom-menu', 
// 						'container_class' => 'custom-menu-class' 
// 					)); 
				?>
			</div> -->
				
		</div><!-- .site-info -->

		
	</footer><!-- #colophon -->
</div><!-- #page -->

<?php wp_footer(); ?>

</body>
</html>
