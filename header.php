<?php
/**
 * The header for our theme
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package nikereal
 */

?>
<!doctype html>
<html <?php language_attributes(); ?>>
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="profile" href="https://gmpg.org/xfn/11">
	<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.0.13/css/all.css" integrity="sha384-DNOHZ68U8hZfKXOrtjWvjxusGo9WQnrNx2sqG0tfsghAvtVlRW3tvkXWZh58N9jp" crossorigin="anonymous">

	<?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>
<?php wp_body_open(); ?>
<div id="page" class="site">
	<header id="masthead" class="site-header">
		<div class="header-top">
			<div class="header-search">
				<?php if ( is_active_sidebar( 'sidebar-6' ) ) : ?>
							<div id="secondary" class="widget-area" role="complementary">
								<?php dynamic_sidebar( 'sidebar-6' ); ?>
							</div>
				<?php endif; ?> 
			</div>

			<div class="cart">
				<?php global $woocommerce; ?>
					<a class="header-cart" href="<?php echo $woocommerce->cart->get_cart_url(); ?>"
						title="<?php _e('Cart View', 'woothemes'); ?>">
						<span class="icon">
						<?php echo sprintf(_n('%d', '%d', $woocommerce->cart->cart_contents_count, 'woothemes'),
						$woocommerce->cart->cart_contents_count);?> </span>
					</a>	
			</div>
		</div>
		<div class="header-bottom">
			<button class="menu-toggle" aria-controls="primary-menu" aria-expanded="false">
					<?php //esc_html_e( 'Primary Menu', 'nikereal' ); ?>
					<i class="fa fa-bars" aria-hidden="true"></i>
			</button>	
			<div class="site-branding__mobile">
				<a href="/">
					<img src="<?php echo get_template_directory_uri(); ?>/images/logo-mobile.png">
				</a>
			</diV>
			<!-- <div class="pre-scrim" data-pre="Scrim"></div> -->
			<div class="site-branding">
				<h1 class="site-branding__title"><?php the_custom_logo();?></h1>

			</div><!-- .site-branding -->
			
		</div>
		<nav id="site-navigation" class="main-navigation">
				
				<?php
				wp_nav_menu(
					array(
						'theme_location' => 'menu-1',
						'menu_id'        => 'primary-menu',
					)
				);
				?>
			</nav><!-- #site-navigation -->
		

		
	</header><!-- #masthead -->
